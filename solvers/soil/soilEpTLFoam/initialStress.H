word is=word(runTime.controlDict().lookup("initialStress"));

if (is == "yes")

{
symmTensorField& sigmaI = sigma.internalField();
     
     // correct internal newSigma
     forAll (sigmaI, cellI)
     {
       scalar zpos = 10 - mesh.C()[cellI].z();
       sigmaI[cellI] = symmTensor(0.658,0,0,0.658,0,1)*(-2e+4)*zpos;
      }

     forAll (sigma.boundaryField(),patchI)
     {
      symmTensorField& sigmaPatch = sigma.boundaryField()[patchI];
      vectorField bc = mesh.C().boundaryField()[patchI];
        forAll (sigmaPatch, faceI)
         {
           scalar zpos = 10 - bc[faceI].z();
           sigmaPatch[faceI] = symmTensor(0.658,0,0,0.658,0,1)*(-2e+4)*zpos;
         }
      }

      /*label patchID = mesh.boundaryMesh().findPatchID("soilStructureInterface");	    
		
      symmTensorField& sigmaPatch = sigma.boundaryField()[patchID];
      forAll (sigmaPatch, faceI)
         {
           
           sigmaPatch[faceI] = symmTensor(-11e3,0,0,-11e3,0,-11e3);
         }*/
		
      sigma.write();
}
