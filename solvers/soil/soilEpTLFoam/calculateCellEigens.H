

#include "mathematicalConstants.H"
    scalar i = 0;
    scalar ii = 0;
    scalar iii = 0;

    if
    (
        (
            mag(newSigmaI[cellI].xy()) + mag(newSigmaI[cellI].xz()) + mag(newSigmaI[cellI].xy())
          + mag(newSigmaI[cellI].yz()) + mag(newSigmaI[cellI].xz()) + mag(newSigmaI[cellI].yz())
        )
      <1e-6*( mag(newSigmaI[cellI].xx()) + mag(newSigmaI[cellI].yy()) + mag(newSigmaI[cellI].zz()))
    )
    {
        // diagonal matrix
        i = newSigmaI[cellI].xx();
        ii = newSigmaI[cellI].yy();
        iii = newSigmaI[cellI].zz();
    }
   /* else if ( 
(
mag(newSigmaI[cellI].xy()) + mag(newSigmaI[cellI].xz()) + mag(newSigmaI[cellI].xy())
          + mag(newSigmaI[cellI].yz()) + mag(newSigmaI[cellI].xz()) + mag(newSigmaI[cellI].yz())+ mag(newSigmaI[cellI].xx()) + mag(newSigmaI[cellI].yy()) + mag(newSigmaI[cellI].zz())
)<1e-4
){
i = newSigmaI[cellI].xx();
        ii = newSigmaI[cellI].yy();
        iii = newSigmaI[cellI].zz();}*/
    else
    {
        scalar a = -newSigmaI[cellI].xx() - newSigmaI[cellI].yy() - newSigmaI[cellI].zz();
        
        scalar b = newSigmaI[cellI].xx()*newSigmaI[cellI].yy() + newSigmaI[cellI].xx()*newSigmaI[cellI].zz() + newSigmaI[cellI].yy()*newSigmaI[cellI].zz()
            - newSigmaI[cellI].xy()*newSigmaI[cellI].xy() - newSigmaI[cellI].xz()*newSigmaI[cellI].xz() - newSigmaI[cellI].yz()*newSigmaI[cellI].yz();

        scalar c = - newSigmaI[cellI].xx()*newSigmaI[cellI].yy()*newSigmaI[cellI].zz() - newSigmaI[cellI].xy()*newSigmaI[cellI].yz()*newSigmaI[cellI].xz()
            - newSigmaI[cellI].xz()*newSigmaI[cellI].xy()*newSigmaI[cellI].yz() + newSigmaI[cellI].xz()*newSigmaI[cellI].yy()*newSigmaI[cellI].xz()
            + newSigmaI[cellI].xy()*newSigmaI[cellI].xy()*newSigmaI[cellI].zz() + newSigmaI[cellI].xx()*newSigmaI[cellI].yz()*newSigmaI[cellI].yz();

        // If there is a zero root
        if (mag(c) < SMALL)
        {
            const scalar disc = Foam::max(sqr(a) - 4*b, 0.0);
            Info<<"what happened!!"<<endl;
            scalar q = -0.5*Foam::sqrt(max(scalar(0), disc));

            i = 0;
            ii = -0.5*a + q;
            iii = -0.5*a - q;
        }
        else
        {
            scalar Q = (a*a - 3*b)/9;
            scalar R = (2*a*a*a - 9*a*b + 27*c)/54;

            scalar R2 = sqr(R);
            scalar Q3 = pow3(Q);

            // Three different real roots
            if (R2 < Q3)
            {
                scalar sqrtQ = Foam::sqrt(Q);
                scalar theta = Foam::acos(R/(Q*sqrtQ));

                scalar m2SqrtQ = -2*sqrtQ;
                scalar aBy3 = a/3;

                i = m2SqrtQ*Foam::cos(theta/3) - aBy3;
                ii = m2SqrtQ*Foam::cos((theta + mathematicalConstant::twoPi)/3)
                    - aBy3;
                iii = m2SqrtQ*Foam::cos((theta - mathematicalConstant::twoPi)/3)
                    - aBy3;
            }
            else
            {
                scalar A = Foam::cbrt(R + Foam::sqrt(R2 - Q3));

                // Three equal real roots
                if (A < SMALL)
                {
                    scalar root = -a/3;
                    i=root;
                    ii=root;
                    iii=root;
                }
                else
                {
                    // Complex roots
                    WarningIn("eigenValues(const symmTensor&)")
                        << "complex eigenvalues detected for symmTensor: " << newSigmaI[cellI]
                        << endl;

                    i=0;
                    ii=0;
                    iii=0;
                }
            }
        }
    }

if (mag(i) > mag(ii))
    {
        Swap(i, ii);
    }

    if (mag(ii) > mag(iii))
    {
        Swap(ii, iii);
    }

    if (mag(i) > mag(ii))
    {
        Swap(i, ii);
    }
    // Sort the eigenvalues into ascending order
   
        sigma_prin[0]=i;
        sigma_prin[1]=ii;
        sigma_prin[2]=iii;

for (int j=0;j<3;j++)
{
         symmTensor A(newSigmaI[cellI] - sigma_prin[j]*I);

    // Calculate the sub-determinants of the 3 components
    scalar sd0 = A.yy()*A.zz() - A.yz()*A.yz();
    scalar sd1 = A.xx()*A.zz() - A.xz()*A.xz();
    scalar sd2 = A.xx()*A.yy() - A.xy()*A.xy();

    scalar magSd0 = mag(sd0);
    scalar magSd1 = mag(sd1);
    scalar magSd2 = mag(sd2);

    // Evaluate the eigenvector using the largest sub-determinant
    if (magSd0 > magSd1 && magSd0 > magSd2 && magSd0 > SMALL)
    {
        vector ev_
        (
            1,
            (A.yz()*A.xz() - A.zz()*A.xy())/sd0,
            (A.yz()*A.xy() - A.yy()*A.xz())/sd0
        );
        ev_ /= mag(ev_);

        ev[j*3]=ev_[0];
        ev[j*3+1]=ev_[1];
        ev[j*3+2]=ev_[2];        
    }
    else if (magSd1 > magSd2 && magSd1 > SMALL)
    {
        vector ev_
        (
            (A.xz()*A.yz() - A.zz()*A.xy())/sd1,
            1,
            (A.xz()*A.xy() - A.xx()*A.yz())/sd1
        );
        ev_ /= mag(ev_);

        ev[j*3]=ev_[0];
        ev[j*3+1]=ev_[1];
        ev[j*3+2]=ev_[2]; 
    }
    else if (magSd2 > SMALL)
    {
        vector ev_
        (
            (A.xy()*A.yz() - A.yy()*A.xz())/sd2,
            (A.xy()*A.xz() - A.xx()*A.yz())/sd2,
            1
        );
        ev_ /= mag(ev_);

       ev[j*3]=ev_[0];
        ev[j*3+1]=ev_[1];
        ev[j*3+2]=ev_[2]; 
    }
    else
    {
        ev[j*3]=0;
        ev[j*3+1]=0;
        ev[j*3+2]=0; 
    }
}



