/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

\*---------------------------------------------------------------------------*/

#include "interpolateTimeVaryingTotalTractionFvPatchVectorField.H"
#include "addToRunTimeSelectionTable.H"
#include "volFields.H"
#include "mathematicalConstants.H"
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

interpolateTimeVaryingTotalTractionFvPatchVectorField::
interpolateTimeVaryingTotalTractionFvPatchVectorField
(
    const fvPatch& p,
    const DimensionedField<vector, volMesh>& iF
)
:
    fixedGradientFvPatchVectorField(p, iF),
   timeSeries_()

{
    fvPatchVectorField::operator=(patchInternalField());
    gradient() = vector::zero;
}


interpolateTimeVaryingTotalTractionFvPatchVectorField::
interpolateTimeVaryingTotalTractionFvPatchVectorField
(
    const interpolateTimeVaryingTotalTractionFvPatchVectorField& tdpvf,
    const fvPatch& p,
    const DimensionedField<vector, volMesh>& iF,
    const fvPatchFieldMapper& mapper
)
:
    fixedGradientFvPatchVectorField(tdpvf, p, iF, mapper),
    timeSeries_(tdpvf.timeSeries_)
{}


interpolateTimeVaryingTotalTractionFvPatchVectorField::
interpolateTimeVaryingTotalTractionFvPatchVectorField
(
    const fvPatch& p,
    const DimensionedField<vector, volMesh>& iF,
    const dictionary& dict
)
:
    fixedGradientFvPatchVectorField(p, iF),
   timeSeries_(dict)
{
    fvPatchVectorField::operator=(patchInternalField());
    gradient() = vector::zero;
}


interpolateTimeVaryingTotalTractionFvPatchVectorField::
interpolateTimeVaryingTotalTractionFvPatchVectorField
(
    const interpolateTimeVaryingTotalTractionFvPatchVectorField& tdpvf
)
:
    fixedGradientFvPatchVectorField(tdpvf),
    timeSeries_(tdpvf.timeSeries_)
{}


interpolateTimeVaryingTotalTractionFvPatchVectorField::
interpolateTimeVaryingTotalTractionFvPatchVectorField
(
    const interpolateTimeVaryingTotalTractionFvPatchVectorField& tdpvf,
    const DimensionedField<vector, volMesh>& iF
)
:
    fixedGradientFvPatchVectorField(tdpvf, iF),
   timeSeries_(tdpvf.timeSeries_)
{}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

void interpolateTimeVaryingTotalTractionFvPatchVectorField::autoMap
(
    const fvPatchFieldMapper& m
)
{
    fixedGradientFvPatchVectorField::autoMap(m);
  
}


// Reverse-map the given fvPatchField onto this fvPatchField
void interpolateTimeVaryingTotalTractionFvPatchVectorField::rmap
(
    const fvPatchVectorField& ptf,
    const labelList& addr
)
{
    fixedGradientFvPatchVectorField::rmap(ptf, addr);
}


// Update the coefficients associated with the patch field
void interpolateTimeVaryingTotalTractionFvPatchVectorField::updateCoeffs()
{
    if (updated())
    {
        return;
    }
    const dictionary& soilProperties =
        db().lookupObject<IOdictionary>("soilProperties");

    dimensionedScalar E(soilProperties.lookup("E"));
    dimensionedScalar nu(soilProperties.lookup("nu"));

    dimensionedScalar mu = E/(2.0*(1.0 + nu));
    dimensionedScalar lambda = nu*E/((1.0 + nu)*(1.0 - 2.0*nu));

    Switch planeStress(soilProperties.lookup("planeStress"));

    if (planeStress)
    {
        lambda = nu*E/((1.0 + nu)*(1.0 - nu));
    }

    vectorField n = patch().nf();
    vectorField Traction(n.size(), vector::zero);
   
    const fvPatchField<symmTensor>& sigma =
        patch().lookupPatchField<volSymmTensorField, symmTensor>("sigma");
    const fvPatchField<scalar>& p =
       patch().lookupPatchField<volScalarField, scalar>("p");
  
   Traction = timeSeries_(this->db().time().timeOutputValue()) -(n & sigma)+n*p;
    
    const fvPatchField<tensor>& graddU =
        patch().lookupPatchField<volTensorField, tensor>("grad(dU)");

 vectorField newGradient = 
      Traction
      - (n & (mu.value()*graddU.T() - (mu + lambda).value()*graddU))
      - n*lambda.value()*tr(graddU);

     const fvPatchField<symmTensor>& deps_p =
        patch().lookupPatchField<volSymmTensorField, symmTensor>("dEpsP");
   //  Info<<"deps_p "<<deps_p<<endl; 
     newGradient +=
	  2*mu.value()*(n & deps_p) + n*tr(deps_p)*lambda.value();

   newGradient /= (2.0*mu + lambda).value();

    gradient() = newGradient;
 
    fixedGradientFvPatchVectorField::updateCoeffs();
}


// Write
void interpolateTimeVaryingTotalTractionFvPatchVectorField::write(Ostream& os) const
{
    fvPatchVectorField::write(os);
    writeEntry("value", os);
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

makePatchTypeField(fvPatchVectorField, interpolateTimeVaryingTotalTractionFvPatchVectorField);

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// ************************************************************************* //
