divDsigmaExp = fvc::div
      (
       mu*(mesh.Sf() & fvc::interpolate(gradDU.T()))
	+ lambda*(mesh.Sf() & I*fvc::interpolate(tr(gradDU)))
	- (mu + lambda)*(mesh.Sf() & fvc::interpolate(gradDU))
       );

