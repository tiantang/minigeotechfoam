/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

\*---------------------------------------------------------------------------*/

#include "stressControlledCyclicLoadFvPatchVectorField.H"
#include "addToRunTimeSelectionTable.H"
#include "volFields.H"
#include "mathematicalConstants.H"
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

stressControlledCyclicLoadFvPatchVectorField::
stressControlledCyclicLoadFvPatchVectorField
(
    const fvPatch& p,
    const DimensionedField<vector, volMesh>& iF
)
:
    fixedGradientFvPatchVectorField(p, iF),
    tractionAmp_(p.size(), vector::zero),
    tractionFre_(p.size(), 0.0),
    tractionOff_(p.size(), vector::zero),
    initialMeanStress_(p.size(), 0.0)

{
    fvPatchVectorField::operator=(patchInternalField());
    gradient() = vector::zero;
}


stressControlledCyclicLoadFvPatchVectorField::
stressControlledCyclicLoadFvPatchVectorField
(
    const stressControlledCyclicLoadFvPatchVectorField& tdpvf,
    const fvPatch& p,
    const DimensionedField<vector, volMesh>& iF,
    const fvPatchFieldMapper& mapper
)
:
    fixedGradientFvPatchVectorField(tdpvf, p, iF, mapper),
    tractionAmp_(tdpvf.tractionAmp_, mapper),
    tractionFre_(tdpvf.tractionFre_, mapper),
    tractionOff_(tdpvf.tractionOff_, mapper),
    initialMeanStress_(tdpvf.initialMeanStress_, mapper)
{}


stressControlledCyclicLoadFvPatchVectorField::
stressControlledCyclicLoadFvPatchVectorField
(
    const fvPatch& p,
    const DimensionedField<vector, volMesh>& iF,
    const dictionary& dict
)
:
    fixedGradientFvPatchVectorField(p, iF),
    tractionAmp_("tractionAmp", dict, p.size()),
    tractionFre_("tractionFre", dict, p.size()),
    tractionOff_("tractionOff", dict, p.size()),
    initialMeanStress_("initialMeanStress", dict, p.size())
{
    fvPatchVectorField::operator=(patchInternalField());
    gradient() = vector::zero;
}


stressControlledCyclicLoadFvPatchVectorField::
stressControlledCyclicLoadFvPatchVectorField
(
    const stressControlledCyclicLoadFvPatchVectorField& tdpvf
)
:
    fixedGradientFvPatchVectorField(tdpvf),
    tractionAmp_(tdpvf.tractionAmp_),
    tractionFre_(tdpvf.tractionFre_),
    tractionOff_(tdpvf.tractionOff_),
    initialMeanStress_(tdpvf.initialMeanStress_)
{}


stressControlledCyclicLoadFvPatchVectorField::
stressControlledCyclicLoadFvPatchVectorField
(
    const stressControlledCyclicLoadFvPatchVectorField& tdpvf,
    const DimensionedField<vector, volMesh>& iF
)
:
    fixedGradientFvPatchVectorField(tdpvf, iF),
    tractionAmp_(tdpvf.tractionAmp_),
    tractionFre_(tdpvf.tractionFre_),
    tractionOff_(tdpvf.tractionOff_),
    initialMeanStress_(tdpvf.initialMeanStress_),
{}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

void stressControlledCyclicLoadFvPatchVectorField::autoMap
(
    const fvPatchFieldMapper& m
)
{
    fixedGradientFvPatchVectorField::autoMap(m);
    tractionAmp_.autoMap(m);
    tractionFre_.autoMap(m);
    tractionOff_.autoMap(m);
    initialMeanStress_.autoMap(m);
}


// Reverse-map the given fvPatchField onto this fvPatchField
void stressControlledCyclicLoadFvPatchVectorField::rmap
(
    const fvPatchVectorField& ptf,
    const labelList& addr
)
{
    fixedGradientFvPatchVectorField::rmap(ptf, addr);

    const stressControlledCyclicLoadFvPatchVectorField& dmptf =
        refCast<const stressControlledCyclicLoadFvPatchVectorField>(ptf);

    tractionAmp_.rmap(dmptf.tractionAmp_, addr);
    tractionFre_.rmap(dmptf.tractionFre_, addr);
    tractionOff_.rmap(dmptf.tractionOff_, addr);
    initialMeanStress_.rmap(dmptf.initialMeanStress_, addr);
}


// Update the coefficients associated with the patch field
void stressControlledCyclicLoadFvPatchVectorField::updateCoeffs()
{
    if (updated())
    {
        return;
    }

    vectorField n = patch().nf();
    vectorField Traction(n.size(), vector::zero);
    symmTensorField tt(n.size(),symmTensor(1,0,0,1,0,1));
    tt*=initialMeanStress_;

    const fvPatchField<symmTensor>& sigma =
        patch().lookupPatchField<volSymmTensorField, symmTensor>("sigma");
      const fvPatchField<scalar>& p =
       patch().lookupPatchField<volScalarField, scalar>("p");

    const fvPatchField<scalar>& K =
        patch().lookupPatchField<volScalarField, scalar>("K");
     const fvPatchField<scalar>& G =
        patch().lookupPatchField<volScalarField, scalar>("G");

   scalar twoPi=2.0*mathematicalConstant::pi;    
   Traction = (tractionOff_+tractionAmp_*Foam::sin(twoPi*tractionFre_*this->db().time().value()))-(n & sigma)+n*p+(n & tt);
    
    const fvPatchField<tensor>& graddU =
        patch().lookupPatchField<volTensorField, tensor>("grad(dU)");

   vectorField newGradient = 
      Traction
      - (n & (G*graddU.T() - (K + 1.0/3.0*G)*graddU))
      - n*(K-2.0/3.0*G)*tr(graddU);


      const fvPatchField<symmTensor>& deps_pD =
        patch().lookupPatchField<volSymmTensorField, symmTensor>("dEpsPD");
     const fvPatchField<scalar>& deps_pV =
        patch().lookupPatchField<volScalarField, scalar>("dEpsPV");
   
     newGradient +=
	  2*G*(n & deps_pD) + n*deps_pV*K;

   newGradient /= (K + 4.0/3.0*G);


    gradient() = newGradient;
 
    fixedGradientFvPatchVectorField::updateCoeffs();
}


// Write
void stressControlledCyclicLoadFvPatchVectorField::write(Ostream& os) const
{
    fvPatchVectorField::write(os);
    tractionAmp_.writeEntry("traction", os);
    tractionFre_.writeEntry("pressure", os);
    writeEntry("value", os);
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

makePatchTypeField(fvPatchVectorField, stressControlledCyclicLoadFvPatchVectorField);

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// ************************************************************************* //
