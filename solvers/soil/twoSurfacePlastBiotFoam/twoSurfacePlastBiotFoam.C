/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNdU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOdUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICdULAR PdURPOSE.  See the GNdU General Public License
    for more details.

    You should have received a copy of the GNdU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 dUSA

Application
    plasticStressedFoam

Description
    Segregated finite-volume solver of porous soils tailored for cyclic loading.

    A seepage equation for the pore fluid pressure (based on Darcy's law and 
    mass conservation), and a total momentum equation for the soil mixture 
    (i.e., soil skeleton + pore fluid which can be a combination of air and 
    water and thus compressible) are solved. 

    The soil skeleton is modelled by a hypoelastic and cyclic plastic 
    constitutive relation. Reference to the theories of the critical state 
    two-surface cyclic plasticity model can be found by Manzari & Dafalias 1997. 

    Under-relaxation is applied for stabilisation in case of large
    plastic strains as well as strong pore pressure-displacement coupling. 

\*---------------------------------------------------------------------------*/

#include "fvCFD.H"
#include "Switch.H"

using namespace Foam;
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{

#   include "setRootCase.H"
#   include "createTime.H"
#   include "createMesh.H"
#   include "readSoilProperties.H"
#   include "createFields.H"


    Info<< "\nCalculating displacement field\n" << endl;

    lduMatrix::debug=0;

    for (runTime++; !runTime.end(); runTime++)
    {
        Info<< "Time: " << runTime.timeName() << nl << endl;

        #   include "readTwoSurfacePlastBiotFoamControls.H"

        int iCorr = 0;
        scalar initialResidual = GREAT;       
        lduMatrix::solverPerformance solverPerfP;
        lduMatrix::solverPerformance solverPerfDU;

        do 
          {
            iCorr++;
   
            p.storePrevIter();
     
            fvScalarMatrix pEqn
			(
				fvm::ddt(p) == fvm::laplacian(Dp, p) - fvc::div(fvc::ddt(Dp2,U))
			);

	    solverPerfP = pEqn.solve(); 

            p.relax();
            dp = p - p.oldTime();
            dp.correctBoundaryConditions();
         
            dU.storePrevIter();
            Kf = fvc::interpolate(K, "K");
            Gf = fvc::interpolate(G, "G");

            #   include "calculateDivDsigmaExp.H"
  
            fvVectorMatrix dUEqn
            (
              //  fvm::d2dt2(rho, dU)
	     //==
                fvm::laplacian(K+4.0/3.0*G, dU, "laplacian(dU)")
               == - divDsigmaExp 
                + fvc::div(2.0*Gf*(mesh.Sf() & fvc::interpolate(dEpsPD)))
                + fvc::div(Kf*(mesh.Sf() & I*fvc::interpolate(dEpsPV)))
                + fvc::grad(dp) 
            ); 
            solverPerfDU = dUEqn.solve();
    
            dU.relax();          
            gradDU = fvc::grad(dU);
     
            # include "twoSurfacePlasticity.H"

            n = e/(1.0+e); 

            Dp = (k/gamma*Kprime/n);
            Dp2 = Kprime/n;

            initialResidual = max(solverPerfP.initialResidual(),solverPerfDU.initialResidual());

            U = U.oldTime()+dU;
            U.correctBoundaryConditions();
           
           if (iCorr % 100 == 0){ Info << "\tTime " << runTime.value()
	         << ", Corrector " << iCorr
	         << ", Solving for " << dU.name()<<" and "<<p.name()
	         << ", maximum residual = " << initialResidual << endl;   
              }          

          } while (initialResidual > convergenceTolerance && iCorr < nCorr);
    
        Info << nl << "Time " << runTime.value() << ", Solving for " << dU.name() 
	     << ", Initial residual = " << initialResidual
	     << ", No outer iterations " << iCorr << endl;

      #  include  "updateFields.H"

        Info << "ExecutionTime = " << runTime.elapsedCpuTime() << " s"
             << "  ClockTime = " << runTime.elapsedClockTime() << " s"
             << nl << endl;
    }


    Info<< "End\n" << endl;
    return(0);
}


// ************************************************************************* //
