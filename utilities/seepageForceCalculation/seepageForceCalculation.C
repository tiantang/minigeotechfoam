/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Description
    Calculates the total seepage forces on a patch by pore pressure integration:
            total force vector
	    total normal force (uplift)
	    total shear force

\*---------------------------------------------------------------------------*/

#include "fvCFD.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
  Foam::argList::validOptions.insert("noMeshUpdate", "");

# include "addTimeOptions.H"

# include "setRootCase.H"

# include "createTime.H"

  // Get times list
  instantList Times = runTime.times();
 
  // set startTime and endTime depending on -time and -latestTime options
# include "checkTimeOptions.H"

  runTime.setTime(Times[startTime], startTime);
  
# include "createMesh.H"

  bool noMeshUpdate = args.optionFound("noMeshUpdate");

  for (label i=startTime; i<endTime; i++)
    {
      runTime.setTime(Times[i], i);

      Info<< "Time = " << runTime.timeName() << endl;

      if(!noMeshUpdate)
	{
	  mesh.readUpdate();
	}

        IOobject pheader
	  (
	   "p",
	   runTime.timeName(),
	   mesh,
	   IOobject::MUST_READ
	   );

        // Check pore pressure exists
        if (pheader.headerOk())
	  {
            Info<< " Reading pore pressure" << endl;
            volScalarField p(pheader, mesh);
 
	    Info << nl;
      
	    vector netSeepageForce = vector::zero;
         
            forAll(mesh.boundary(), patchID)
            {
              vectorField n = mesh.boundary()[patchID].nf();
              const vectorField& Sf = mesh.boundary()[patchID].Sf();
              const scalarField& pPatch = p.boundaryField()[patchID];
   
              vector totalSeepageForce = sum(Sf*pPatch);
    
              netSeepageForce += totalSeepageForce; 
              scalar upliftForce = sum(n & (Sf * pPatch));
              vector seepageShearForce = sum((I -sqr(n)) & (Sf * pPatch));
              Info << "Patch: " << mesh.boundary()[patchID].name() << nl
		     << "\tTotal Seepage Force:\t\t" << totalSeepageForce << " N\n"
		     << "\tTotal Uplift Force:\t" << upliftForce <<  " N\n"
		     << "\tTotal Seepage Shear Force:\t" << seepageShearForce <<  " N\n"<<endl; 
              }
	      Info << nl << "Net seepage force on model is " << netSeepageForce<< " N" << endl;
	  }
    }
  
  Info << nl << "End" << endl;
  
  return 0;
}


// ************************************************************************* //
