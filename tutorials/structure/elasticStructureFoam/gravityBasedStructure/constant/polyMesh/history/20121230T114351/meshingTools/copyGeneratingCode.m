function copyGeneratingCode(meshDir,toolboxDir,mfile)

histDir = [meshDir filesep 'history'];
if exist(histDir) ~= 7
    mkdir(histDir);
end
time = datestr(now,30);
copyDir = [histDir filesep time];
mkdir(copyDir)
copyfile(toolboxDir,[copyDir filesep 'meshingTools'])
copyfile([mfile '.m'],[copyDir filesep mfile '.m'] )

