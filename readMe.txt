===== Description =====

miniGeotechFoam: A package of soil mechanics solvers aimed for geotechnical engineering applications.

The included soil solvers employ the finite volume method (FVM) to numerically approximate the various soil mechanical responses upon loading, including the pore pressure, soil displacements, strains and stresses.

The global solution algorithm implemented in all the soil solvers is the same, namely the segregated FVM + fixed point iteration + under-relaxtion. 

The main difference between the soil solvers lies in the specific constitutive model adopted as well as the stress integration procedure applied. Currently, the included soil constitutive models are suitable for elasto-plasticity (elastoPlasticBiotFoam), cyclic plasticity (twoSurfacePlastBiotFoam), anisotropy (anisotropicBiotFoam) and large strains (soilEpTLFoam). Other soil consitutive models can be added further in a straightforward manner. 

Fixed and time-varying total traction boundary conditions are accompanied in each soil solver for boundary value problems in geomechanics.   

In addition to the soil solvers, an elastic structure solver together with a socalled "externalPatchToPatchMapping" and a "timeVaryingPressureLoad" boundary condition are also included in the package. The externalPatchToPatchMapping bc can map data from an external stand-alone patch onto the boundary patch, with time interporlation. The structure solver and the bcs are suitable for wave-structure interaction problems: having wave pressures on the wave-structure interface from CFD calculation or other external sources as input, then computing the structure response.            

The development of the solvers is a part of a 3-year PhD study of Tian Tang at the Civil Engineering Department, Technical Univeristy of Denmark (DTU). A number of people have made acknowleged contributions to the work, mainly including Asso.Prof. Ole Hededal from DTU, Dr, Philip Cardiff from University College Dublin and Dr. Johan Roenby from DHI.   

===== Documentation ===== 
The following two paper describes the implementation procedure of the soil solvers:

T.Tang, O.Hededal & P.Cardiff (2014). On finite volume method implementation of poro-elasto-plasticity soil model. Accepted by International Journal for Numerical and Analytical Methods in Geomechanics. Online version will be available soon. 

T.Tang & O.Hededal (2014). Simulation of pore pressure accumulation under cyclic loading using finite volume method. Proceedings of the 8th European Conference on Numerical Methods in Geotechnical Engineering (NUMGE14), Volume 2, Pages 1301-1306. 

===== Note =====
For your information, the papers mentioned above can be found in the corresponing solver directory, i.e. the first paper for elastoPlasticBiotFoam and the second paper for twoSurfacePlastBiotFoam.  




